package com.gbraille.forca;

public class QuestionClass {
	private long id;
	private String answer;
	private String question;

	public long getId() {
	    return id;
	}
	
	public void setId(long id) {
	    this.id = id;
	}
	
	public String getAnswer() {
	    return answer;
	}
	
	public void setAnswer(String answer) {
	    this.answer = answer;
	}
	
	public String getQuestion() {
	    return question;
	}
	
	public void setQuestion(String dica) {
	    this.question = dica;
	}	
	
	// Will be used by the ArrayAdapter in the ListView
	@Override
	public String toString() {
		return answer;
	}
}